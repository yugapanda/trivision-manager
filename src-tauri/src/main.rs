#![cfg_attr(
    all(not(debug_assertions), target_os = "windows"),
    windows_subsystem = "windows"
)]

use rosc::encoder;
use rosc::{OscMessage, OscPacket, OscType};
use std::net::{AddrParseError, SocketAddrV4, UdpSocket};
use std::str::FromStr;

struct UDPState(UdpSocket);

fn get_addr_from_arg(arg: &str) -> Result<SocketAddrV4, AddrParseError> {
    SocketAddrV4::from_str(arg)
}

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct RequestBody {
    message: String,
}

#[derive(Debug, serde::Serialize, serde::Deserialize)]
pub struct SendBody {
    addr: String,
    message: String,
    path: String,
}

fn main() {
    let sock = UdpSocket::bind(get_addr_from_arg("0.0.0.0:13000").unwrap()).unwrap();

    tauri::Builder::default()
        .manage(UDPState(sock))
        .invoke_handler(tauri::generate_handler![echo, send])
        .run(tauri::generate_context!())
        .expect("error");
}

#[tauri::command]
fn echo(invoke_message: RequestBody) -> String {
    invoke_message.message
}

#[tauri::command]
fn send(invoke_message: SendBody, sock: tauri::State<UDPState>) -> bool {
    let res = get_addr_from_arg(&invoke_message.addr)
    .map_err(|e| println!("error: {:?}", e))
    .ok()
    .and_then(|to_addr| 
        encoder::encode(&OscPacket::Message(OscMessage {
            addr: invoke_message.path,
            args: vec![OscType::String(invoke_message.message)],
        }))
        .map_err(|e| println!("error: {:?}", e))
        .ok()
        .and_then(|msg_buff| sock.0.send_to(&msg_buff, &to_addr).map_err(|e| println!("error: {:?}", e)).ok()));

      match res {
        Some(_) => true,
        None => false 
    }
}
