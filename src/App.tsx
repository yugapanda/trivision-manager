import { tauri } from "@tauri-apps/api";
import React, { FC, useEffect, useState } from "react";
import "./App.css";
// import { tauri } from "@tauri-apps/api";

const App: FC = () => {
  const [addrL, setAdderL] = useState("");
  const [addrC, setAdderC] = useState("");
  const [addrR, setAdderR] = useState("");
  const [addrLOK, setAdderLOK] = useState(false);
  const [addrCOK, setAdderCOK] = useState(false);
  const [addrROK, setAdderROK] = useState(false);

  const [isPlay, setIsPlay] = useState(false);

  const getIpClassName = (addr: string) => {
    if(addr === "") {
      return "ip"
    }
    return "ip not-empty"
  }

  const send = async (addr: string, message: string, path: string, set: (b: boolean) => void) => {
    try {
      const response = await tauri.invoke<boolean>("send", {
        invokeMessage: { addr, message, path },
      });
      console.log(response);
      if (response !== null) {
        set(response);
      }
    } catch (e) {
      console.log(e);
    }
  };

  const sendAll = async (message: string, path: string) => {
    try {
      const res1 = tauri.invoke<boolean>("send", {
        invokeMessage: { addr: addrL, message, path },
      });
      const res2 = tauri.invoke<boolean>("send", {
        invokeMessage: { addr: addrC, message, path },
      });
      const res3 = tauri.invoke<boolean>("send", {
        invokeMessage: { addr: addrR, message, path },
      });
      const ok = await Promise.all([res1, res2, res3]);
    } catch (e) {
      console.log(e);
    }
  };

  const start = async () => {
    const ok = await sendAll("", "/start");
    setIsPlay(true);
  };
  const pause = async () => {
    const ok = await sendAll("", "/pause");
    setIsPlay(false);
  };

  return (
    <div className="App">
      <div className="player"></div>
      {isPlay ? (
        <button className="button paused" onClick={() => pause()} />
      ) : (
        <button className="button" onClick={() => start()} />
      )}
      <div className="ip-inputs">
        <input
          onChange={(e) => setAdderL(e.currentTarget.value)}
          type="text"
          className={getIpClassName(addrL)}
        />
        <input
          onChange={(e) => setAdderC(e.currentTarget.value)}
          type="text"
          className={getIpClassName(addrC)}
        />
        <input
          onChange={(e) => setAdderR(e.currentTarget.value)}
          type="text"
          className={getIpClassName(addrR)}
        />
      </div>
    </div>
  );
};

export default App;
